package com.formula.distribution.idempotent.annotation;

/**
 * 生成token
 *
 * @author yang yang
 * @create 2018/12/10
 */
public @interface ExtApiToken {
    String value();
}
