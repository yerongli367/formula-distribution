package com.formula.distribution.shorter.generator;

import com.formula.distribution.shorter.shorterurl.ShorterUrlString;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/19
 * @introduce 短连接生成的简单实现
 **/
public class ShorterUrlGeneratorSimple extends AbstractUrlShorterGenerator<ShorterUrlString> {

    @Override
    public ShorterUrlString generator(String url) {
        String shorter = super.generator.generate(url);
        while (shorterStorage.get(shorter) != null) {
            shorter = generator.generate(url);
        }
        ShorterUrlString newShorter = new ShorterUrlString(shorter);
        shorterStorage.save(url, newShorter);
        return newShorter;
    }
}
