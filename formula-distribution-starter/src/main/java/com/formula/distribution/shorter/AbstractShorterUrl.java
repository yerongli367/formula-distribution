package com.formula.distribution.shorter;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/18
 * @introduce
 **/
public abstract class AbstractShorterUrl<T> implements ShorterGetter<T> {


    private String shorter;


    @Override
    public String getShorter() {
        return shorter;
    }

    public void setShorter(String shorter) {
        this.shorter = shorter;
    }
}
