package com.formula.distribution.shorter;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/18
 * @introduce 短连接存储
 **/
public interface ShorterStorage<T extends ShorterGetter<T>> {


    String get(String shorterUrl);


    void clean(String url);

    void cleanShorter(String shorterUrl);

    void save(String url, T shorter);

    void clean();

}
