package com.formula.distribution.shorter;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/18
 * @introduce
 **/
public interface ShorterGetter<T> {

    String getShorter();
}
