package com.formula.distribution.shorter.shorterurl;


import com.formula.distribution.shorter.AbstractShorterUrl;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/18
 * @introduce
 **/
public class ShorterUrlString extends AbstractShorterUrl<ShorterUrlString> {


    public ShorterUrlString(String shorterUrl) {
        setShorter(shorterUrl);
    }
}
