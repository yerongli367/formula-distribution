package com.formula.distribution.shorter;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/19
 * @introduce 随机字符串发生器
 **/
public interface StringGenerator {


    String generate(String url);


    void setLength(int length);

}
