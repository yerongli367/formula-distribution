package com.formula.distribution.shorter.generator;

import com.formula.distribution.shorter.shorterurl.ShorterUrlWithTimeOut;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/19
 * @introduce 带超时时间的短连接生成器
 **/
public class ShorterUrlGeneratorWithTimeOut extends AbstractUrlShorterGenerator<ShorterUrlWithTimeOut> {


    private Long timeOut;


    public Long getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Long timeOut) {
        this.timeOut = timeOut;
    }

    @Override
    public ShorterUrlWithTimeOut generator(String url) {
        String shorterUrl = super.generator.generate(url);
        while (shorterStorage.get(shorterUrl) != null) {
            shorterUrl = super.generator.generate(url);
        }
        ShorterUrlWithTimeOut shorterUrlWithTimeOut = new ShorterUrlWithTimeOut(shorterUrl, timeOut);
        shorterStorage.save(url, shorterUrlWithTimeOut);
        return shorterUrlWithTimeOut;
    }
}
