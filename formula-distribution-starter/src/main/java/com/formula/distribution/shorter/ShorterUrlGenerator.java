package com.formula.distribution.shorter;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/18
 * @introduce 短连接生成
 **/
public interface ShorterUrlGenerator<T extends ShorterGetter<T>> {

    /**
     * 生成短连接
     *
     * @param url
     * @return
     */
    T generator(String url);
}
