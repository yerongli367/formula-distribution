package com.formula.distribution.shorter.shorterurl;

import com.formula.distribution.shorter.AbstractShorterUrl;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/18
 * @introduce 带超时时间的短连接
 **/
public class ShorterUrlWithTimeOut extends AbstractShorterUrl<ShorterUrlWithTimeOut> {

    /**
     * 超时时间
     */
    private Long timeOut;


    public Long getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Long timeOut) {
        this.timeOut = timeOut;
    }


    public ShorterUrlWithTimeOut(String shorterUrl, Long timeOut) {
        setShorter(shorterUrl);
        setTimeOut(timeOut);
    }
}
