package com.formula.distribution.shorter.generator;

import com.formula.distribution.shorter.AbstractShorterUrl;
import com.formula.distribution.shorter.ShorterStorage;
import com.formula.distribution.shorter.ShorterUrlGenerator;
import com.formula.distribution.shorter.StringGenerator;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/19
 * @introduce
 **/

public abstract class AbstractUrlShorterGenerator<T extends AbstractShorterUrl<T>> implements ShorterUrlGenerator<T> {

    public  StringGenerator generator;
    public ShorterStorage<T> shorterStorage;

    public ShorterStorage<T> getShorterStorage() {
        return shorterStorage;
    }

    public void setShorterStorage(ShorterStorage<T> shorterStorage) {
        this.shorterStorage = shorterStorage;
    }

    public StringGenerator getGenerator() {
        return generator;
    }

    public void setGenerator(StringGenerator generator) {
        this.generator = generator;
    }

}
