package com.formula.distribution.shorter.generator;

import com.formula.distribution.shorter.shorterurl.ShorterUrlString;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/19
 * @introduce 短连接生成的默认实现
 **/
public class UrlShorterGeneratorSimple extends AbstractUrlShorterGenerator<ShorterUrlString> {


    @Override
    public ShorterUrlString generator(String url) {
        String shorter = generator.generate(url);
        while (shorterStorage.get(shorter) != null) {
            shorter = generator.generate(url);
        }
        ShorterUrlString newShorter = new ShorterUrlString(shorter);
        shorterStorage.save(url, newShorter);
        return newShorter;
    }
}
