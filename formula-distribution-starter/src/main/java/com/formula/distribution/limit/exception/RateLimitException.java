package com.formula.distribution.limit.exception;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/13 14:18
 * @introduce
 **/
public class RateLimitException extends RuntimeException {

    private String message;

    public RateLimitException(String message) {
        super(message);
        this.message = message;
    }


    public RateLimitException() {
        super();
    }


    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}