package com.formula.distribution.limit.config;

import com.formula.distribution.limit.enums.RateLimitConstantEnums;
import com.formula.distribution.limit.ratelimit.AbstractRateLimiter;
import com.formula.distribution.limit.ratelimit.impl.RedisRateLimiterCounterImpl;
import com.formula.distribution.limit.ratelimit.impl.RedisRateLimiterTokenBucketImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.scripting.support.ResourceScriptSource;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/14 11:09
 * @introduce
 **/
@Slf4j
@Configuration
@ComponentScan(basePackages = "com.formula.distribution.limit")
public class EnableRateLimitConfiguration {

    @Bean
    @ConditionalOnMissingBean(name = "redisTemplate")
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<Object, Object> template = new RedisTemplate<Object, Object>();
        template.setConnectionFactory(connectionFactory);
        template.setKeySerializer(new StringRedisSerializer());
        template.afterPropertiesSet();
        return template;
    }

    @Bean(name = "rateLimiter")
    @ConditionalOnProperty(prefix = RateLimitConstantEnums.PREFIX, name = "algorithm", havingValue = "token")
    public AbstractRateLimiter tokenRateLimiter() {
        DefaultRedisScript<Long> consumeRedisScript = new DefaultRedisScript();
        consumeRedisScript.setResultType(Long.class);
        consumeRedisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("script/redis-ratelimiter-tokenBucket.lua")));
        return new RedisRateLimiterTokenBucketImpl(consumeRedisScript);
    }

    @Bean(name = "rateLimiter")
    @ConditionalOnProperty(prefix = RateLimitConstantEnums.PREFIX, name = "algorithm", havingValue = "counter", matchIfMissing = true)
    public AbstractRateLimiter counterRateLimiter() {
        DefaultRedisScript<Long> redisScript = new DefaultRedisScript();
        redisScript.setResultType(Long.class);
        redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("script/redis-ratelimiter-counter.lua")));
        return new RedisRateLimiterCounterImpl(redisScript);
    }


}
