package com.formula.distribution.limit.algorithm;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/13 14:01
 * @introduce 限流接口
 **/
public interface RateLimitAlgorihm {


    /**
     * 限流
     *
     * @param key
     * @param limit
     * @param lrefreshInterval
     * @param tokenBucketStepNum
     * @param tokenBucketTimeInterva
     * @param message
     */
    void handler(String key, long limit, long lrefreshInterval, long tokenBucketStepNum, long tokenBucketTimeInterva, String message);

}
