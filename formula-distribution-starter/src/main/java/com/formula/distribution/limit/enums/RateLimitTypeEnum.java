package com.formula.distribution.limit.enums;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/13 11:52
 * @introduce 限流类型
 **/
public enum RateLimitTypeEnum {


    /**
     * 针对所有请求进行限制
     */
    ALL,
    /**
     * 针对ip 进行限制
     */
    IP,
    /**
     * 自定义限流
     */
    CUSTOM;

}
