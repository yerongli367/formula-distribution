package com.formula.distribution.limit.annotation;

import com.formula.distribution.limit.config.EnableRateLimitConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/14 10:48
 * @introduce 开启限流开关
 **/
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(EnableRateLimitConfiguration.class)
public @interface EnableRateLimit {
}
