package com.formula.distribution.limit.annotation;

import com.formula.distribution.limit.enums.RateLimitTypeEnum;

import java.lang.annotation.*;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/13 11:42
 * @introduce 限流注解
 **/
@Target(ElementType.METHOD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RateLimit {


    /**
     * 限流类型
     *
     * @return
     */
    RateLimitTypeEnum rateLimitType() default RateLimitTypeEnum.ALL;


    /**
     * 限流的次数  单位为S
     *
     * @return
     */
    long limit() default 10;


    /**
     * 时间间隔 单位为S
     *
     * @return
     */
    long refreshInterval() default 60;


    /****************************************************/

    /**
     * @return 向令牌桶中添加数据的时间间隔, 以秒为单位。默认值10秒
     */
    long tokenBucketTimeInterval() default 10;

    /**
     * @return 每次为令牌桶中添加的令牌数量。默认值5个
     */
    long tokenBucketStepNum() default 5;


    /**
     * 限流的时候的异常信息
     *
     * @return
     */
    String message() default "您的请求太快了";

}
