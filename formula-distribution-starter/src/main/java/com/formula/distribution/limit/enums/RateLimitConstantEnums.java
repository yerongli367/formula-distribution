package com.formula.distribution.limit.enums;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/13 14:07
 * @introduce 全局静态常量类
 **/
public interface RateLimitConstantEnums {

    /**
     * 配置rateLimit的前缀
     */
    String PREFIX = "rateLimit";
    /**
     * 集群模式指定slot的hash tag
     */
    String HASH_TAG = "{syj}";

    /**
     * 自定义拦截方式时的key
     */
    String CUSTOM = "rateLimit-custom";
    /**
     * redis返回错误信息
     */
    String REDIS_ERROR = "-1";

}
