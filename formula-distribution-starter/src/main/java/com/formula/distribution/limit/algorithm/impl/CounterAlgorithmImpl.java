package com.formula.distribution.limit.algorithm.impl;

import com.formula.distribution.limit.algorithm.RateLimitAlgorihm;
import com.formula.distribution.limit.enums.RateLimitConstantEnums;
import com.formula.distribution.limit.ratelimit.AbstractRateLimiter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;


/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/13 14:04
 * @introduce 计数器 限流
 **/
@Service
@DependsOn("rateLimiter")
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = RateLimitConstantEnums.PREFIX, name = "algorithm", havingValue = "counter", matchIfMissing = true)
public class CounterAlgorithmImpl implements RateLimitAlgorihm {


    @NonNull
    private AbstractRateLimiter rateLimiter;


    @Override
    public void handler(String key, long limit, long lrefreshInterval, long tokenBucketStepNum, long tokenBucketTimeInterva, String message) {
        rateLimiter.rateLimit(key, limit, lrefreshInterval, tokenBucketStepNum, tokenBucketTimeInterva, message);
        ;
    }
}
