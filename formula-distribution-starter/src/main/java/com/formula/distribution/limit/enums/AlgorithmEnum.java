package com.formula.distribution.limit.enums;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/13 11:48
 * @introduce 限流算法
 **/
public enum AlgorithmEnum {


    /**
     * 计数器
     */
    Counter,

    /**
     * 令牌桶
     */
    TokenBucket;

}
