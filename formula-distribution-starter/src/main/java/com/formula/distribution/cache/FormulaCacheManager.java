package com.formula.distribution.cache;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/11/30 12:14
 * @introduce
 **/

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.support.spring.GenericFastJsonRedisSerializer;
import com.formula.distribution.cache.annotation.CacheExpireTime;
import com.formula.distribution.cache.annotation.Random;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.data.redis.cache.RedisCache;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.util.ReflectionUtils;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.Callable;

/**
 * Redis 容易出现缓存问题（超时、Redis 宕机等），当使用 spring cache 的注释 Cacheable、Cacheput 等处理缓存问题时，
 * 我们无法使用 try catch 处理出现的异常，所以最后导致结果是整个服务报错无法正常工作。
 * 通过自定义 TedisCacheManager 并继承 RedisCacheManager 来处理异常可以解决这个问题。
 */
@Slf4j
public class FormulaCacheManager extends RedisCacheManager implements ApplicationContextAware, InitializingBean {
    private ApplicationContext applicationContext;

    private Map<String, RedisCacheConfiguration> initialCacheConfiguration = new LinkedHashMap<>();

    private Map<String, CacheExpireTime> initialCacheTime = new LinkedHashMap<>();
//
//    /**
//     * key serializer
//     */
//    public static final StringRedisSerializer STRING_SERIALIZER = new StringRedisSerializer();
//
//    /**
//     * value serializer
//     * <pre>
//     *     使用 FastJsonRedisSerializer 会报错：java.lang.ClassCastException
//     *     FastJsonRedisSerializer<Object> fastSerializer = new FastJsonRedisSerializer<>(Object.class);
//     * </pre>
//     */
//    public static final GenericFastJsonRedisSerializer FASTJSON_SERIALIZER = new GenericFastJsonRedisSerializer();
//
//    /**
//     * key serializer pair
//     */
//    public static final RedisSerializationContext.SerializationPair<String> STRING_PAIR = RedisSerializationContext
//            .SerializationPair.fromSerializer(STRING_SERIALIZER);
//    /**
//     * value serializer pair
//     */
//    public static final RedisSerializationContext.SerializationPair<Object> FASTJSON_PAIR = RedisSerializationContext
//            .SerializationPair.fromSerializer(FASTJSON_SERIALIZER);

    public FormulaCacheManager(RedisCacheWriter cacheWriter, RedisCacheConfiguration defaultCacheConfiguration) {
        super(cacheWriter, defaultCacheConfiguration);
    }

    // public TedisCacheManager(RedisCacheWriter cacheWriter, RedisCacheConfiguration defaultCacheConfiguration,
    //                          Map<String, RedisCacheConfiguration> initialCacheConfigurations) {
    //     super(cacheWriter, defaultCacheConfiguration, initialCacheConfigurations);
    // }

    @Override
    public Cache getCache(String name) {
        Cache cache = super.getCache(name);
//
//        CacheExpireTime cacheExpireTime = initialCacheTime.get(cache.getName());
//        if (null != cacheExpireTime) {
//            //计算失效时间
//            long expireTime = countExpireTime(cacheExpireTime);
//            log.error("{}的实时设置失效时间为{}/s", cache.getName(), expireTime);
//            RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig()
//                    .entryTtl(Duration.ofSeconds(2000L));
//            initialCacheConfiguration.put(cache.getName(), config);
////            Map<String, RedisCacheConfiguration> cacheConfigurations = super.getCacheConfigurations();
////            if (null == cacheConfigurations) {
////                cacheConfigurations = new HashMap<>();
////            }
//////            Map<String, RedisCacheConfiguration>  map = new HashMap<>();
//////            map.put(cache.getName(),config);
//////            cacheConfigurations.putAll(map);
//////            initialCacheConfiguration.put(cache.getName(), config);
//////            loadCaches();
//        }

        return new RedisCacheWrapper(cache);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void afterPropertiesSet() {
        String[] beanNames = applicationContext.getBeanNamesForType(Object.class);
        for (String beanName : beanNames) {
            final Class clazz = applicationContext.getType(beanName);
            add(clazz);
        }
        super.afterPropertiesSet();
    }

    @Override
    protected Collection<RedisCache> loadCaches() {
        List<RedisCache> caches = new LinkedList<>();
        for (Map.Entry<String, RedisCacheConfiguration> entry : initialCacheConfiguration.entrySet()) {

            caches.add(super.createRedisCache(entry.getKey(), entry.getValue()));
        }
        return caches;
    }

    /**
     * 计算失效时间   优先使用 random
     *
     * @param cacheExpireTime
     * @return
     */
    private static long countExpireTime(CacheExpireTime cacheExpireTime) {
        Random[] randoms = cacheExpireTime.random();
        Long expire = null;
        if (null != randoms && randoms.length > 0) {
            Random random = randoms[0];
            if (random.max() <= 0 || random.min() < 0) {
                throw new IllegalArgumentException("随机时间设置不合理");
            }
            if (random.min() > random.max()) {

                throw new IllegalArgumentException("最小时间不能大于最大时间");
            }
            expire = RandomUtil.randomLong(random.min(), random.max());
        } else {
            long value = cacheExpireTime.value();
            if (value <= 0) {
                throw new IllegalArgumentException("失效时间设置不合理");
            }
            expire = value;
        }
        return expire;
    }


    private void add(final Class clazz) {
        ReflectionUtils.doWithMethods(clazz, method -> {
            ReflectionUtils.makeAccessible(method);
            CacheConfig cacheConfig = AnnotationUtils.findAnnotation(clazz, CacheConfig.class);


            // 方法上的注解
            CacheExpireTime cacheExpireMethod = AnnotationUtils.findAnnotation(method, CacheExpireTime.class);
            //  类上的注解
            CacheExpireTime cacheExpireClazz = AnnotationUtils.findAnnotation(clazz, CacheExpireTime.class);
            if (null == cacheExpireMethod && null == cacheExpireClazz) {
                return;
            }

            //计算失效时间
            // 方法上的缓存时间
            Cacheable cacheable = AnnotationUtils.findAnnotation(method, Cacheable.class);
            //  这里设置缓存时间
            //  1. 作用于方法的时候：
            // 当方法缓存存在时,设置方法缓存,当方法缓存不存在的时候,使用类的缓存
            // 当类的缓存也不存在的时候,使用默认的缓存
            // 2. 作用与类的时候,直接作用于类下的所有使用@Cacheable注解的方法

            if (cacheable != null) {
                if (null != cacheExpireMethod) {
                    //作用于方法
                    add(cacheable.cacheNames(), cacheExpireMethod);
                } else if (null == cacheExpireMethod && null != cacheExpireClazz) {
                    add(cacheable.cacheNames(), cacheExpireClazz);
                }

                return;
            }
            Caching caching = AnnotationUtils.findAnnotation(method, Caching.class);
            if (caching != null) {
                Cacheable[] cs = caching.cacheable();
                if (cs.length > 0) {
                    for (Cacheable c : cs) {
                        if (null != cacheExpireMethod) {
                            //作用于方法
                            add(c.cacheNames(), cacheExpireMethod);
                        } else if (null == cacheExpireMethod && null != cacheExpireClazz) {
                            add(c.cacheNames(), cacheExpireClazz);
                        }
                    }
                }
            }

        }, method -> (null != AnnotationUtils.findAnnotation(method, CacheExpireTime.class) || null != AnnotationUtils.findAnnotation(clazz, CacheExpireTime.class)));
    }

    private void add(String[] cacheNames, CacheExpireTime cacheExpire) {
        if (null == cacheExpire) {
            return;
        }
        for (String cacheName : cacheNames) {
            if (cacheName == null || "".equals(cacheName.trim())) {
                continue;
            }
            long expire = countExpireTime(cacheExpire);
            log.debug("cacheName: {}, expire: {}", cacheName, expire);
            if (expire >= 0) {
//                // 缓存配置
//
                RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig()
                        .entryTtl(Duration.ofSeconds(expire));
//                        .disableCachingNullValues()800070410
                        // .prefixKeysWith(cacheName)
//                        .serializeKeysWith(STRING_PAIR)
//                        .serializeValuesWith(FASTJSON_PAIR);
                initialCacheConfiguration.put(cacheName, config);
//                initialCacheTime.put(cacheName, cacheExpire);
            } else {
                log.warn("{} use default expiration.", cacheName);
            }
        }
    }

    protected static class RedisCacheWrapper implements Cache {
        private final Cache cache;

        RedisCacheWrapper(Cache cache) {
            this.cache = cache;

        }

        @Override
        public String getName() {
            log.debug("name: {}", cache.getName());
            try {
                return cache.getName();
            } catch (Exception e) {
                log.error("getName ---> errmsg: {}", e.getMessage(), e);
                return null;
            }
        }

        @Override
        public Object getNativeCache() {
            log.debug("nativeCache: {}", cache.getNativeCache());
            try {
                return cache.getNativeCache();
            } catch (Exception e) {
                log.error("getNativeCache ---> errmsg: {}", e.getMessage(), e);
                return null;
            }
        }

        @Override
        public ValueWrapper get(Object o) {
            log.debug("get ---> o: {}", o);
            try {
                return cache.get(o);
            } catch (Exception e) {
                log.error("get ---> o: {}, errmsg: {}", o, e.getMessage(), e);
                return null;
            }
        }

        @Override
        public <T> T get(Object o, Class<T> aClass) {
            log.debug("get ---> o: {}, clazz: {}", o, aClass);
            try {
                return cache.get(o, aClass);
            } catch (Exception e) {
                log.error("get ---> o: {}, clazz: {}, errmsg: {}", o, aClass, e.getMessage(), e);
                return null;
            }
        }

        @Override
        public <T> T get(Object o, Callable<T> callable) {
            log.debug("get ---> o: {}", o);
            try {
                return cache.get(o, callable);
            } catch (Exception e) {
                log.error("get ---> o: {}, errmsg: {}", o, e.getMessage(), e);
                return null;
            }
        }

        @Override
        public void put(Object o, Object o1) {
            log.debug("put ---> o: {}, o1: {}", o, o1);
            try {

                cache.put(o, o1);
            } catch (Exception e) {
                log.error("put ---> o: {}, o1: {}, errmsg: {}", o, o1, e.getMessage(), e);
            }
        }

        @Override
        public ValueWrapper putIfAbsent(Object o, Object o1) {
            log.debug("putIfAbsent ---> o: {}, o1: {}", o, o1);
            try {
                return cache.putIfAbsent(o, o1);
            } catch (Exception e) {
                log.error("putIfAbsent ---> o: {}, o1: {}, errmsg: {}", o, o1, e.getMessage(), e);
                return null;
            }
        }

        @Override
        public void evict(Object o) {
            log.debug("evict ---> o: {}", o);
            try {
                cache.evict(o);
            } catch (Exception e) {
                log.error("evict ---> o: {}, errmsg: {}", o, e.getMessage(), e);
            }
        }

        @Override
        public void clear() {
            log.debug("clear");
            try {
                cache.clear();
            } catch (Exception e) {
                log.error("clear ---> errmsg: {}", e.getMessage(), e);
            }
        }
    }
}