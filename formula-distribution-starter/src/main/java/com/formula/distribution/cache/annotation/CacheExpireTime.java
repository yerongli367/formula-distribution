package com.formula.distribution.cache.annotation;

import java.lang.annotation.*;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/11/29 14:01
 * @introduce 过期时间
 **/

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CacheExpireTime {


    /**
     * 默认的时间
     *
     * @return
     */
    long value() default 0L;

    Random[] random() default {};
}
