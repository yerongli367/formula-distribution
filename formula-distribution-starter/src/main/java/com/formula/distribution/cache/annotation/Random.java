package com.formula.distribution.cache.annotation;

import java.lang.annotation.*;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/1 9:53
 * @introduce 随机失效时间
 **/
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Random {


    /**
     * 最大的时间
     *
     * @return
     */
    long max() default 0L;

    /**
     * 最小的时间
     *
     * @return
     */
    long min() default 0L;


}
