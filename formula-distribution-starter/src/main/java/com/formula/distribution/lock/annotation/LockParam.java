package com.formula.distribution.lock.annotation;

import java.lang.annotation.*;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/22
 * @introduce 参数注解
 **/
@Documented
@Inherited
@Target({ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface LockParam {

    String value() default "";

}
