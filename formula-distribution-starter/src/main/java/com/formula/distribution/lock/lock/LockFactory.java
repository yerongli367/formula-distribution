package com.formula.distribution.lock.lock;

import com.formula.distribution.lock.annotation.Lock;
import com.formula.distribution.lock.core.LockInfoProvider;
import com.formula.distribution.lock.entity.LockInfo;
import com.formula.distribution.lock.entity.LockType;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/25
 * @introduce
 **/
@Slf4j
public class LockFactory {


    @Autowired
    private RedissonClient redissonClient;


    @Autowired
    private LockInfoProvider lockInfoProvider;


    public LockInterface getLock(ProceedingJoinPoint point, Lock lock) {
        LockInfo lockInfo = lockInfoProvider.get(point, lock);
        switch (lockInfo.getLockType()) {
            case LockType.REENTRANT:
                return new ReentrantLock(lockInfo, redissonClient);
            case LockType.FAIR:
                return new FairLock(lockInfo, redissonClient);
            case LockType.READ:
                return new ReadLocK(lockInfo, redissonClient);
            case LockType.WRITE:
                return new WriteLock(lockInfo, redissonClient);
            default:
                return new ReentrantLock(lockInfo, redissonClient);

        }
    }


}
