package com.formula.distribution.lock.lock;

import com.formula.distribution.lock.entity.LockInfo;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

import java.util.concurrent.TimeUnit;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/26
 * @introduce
 **/
public class ReentrantLock implements LockInterface {
    private static volatile RLock rLock;


    private final LockInfo lockInfo;


    private RedissonClient redissonClient;


    public ReentrantLock(LockInfo lockInfo, RedissonClient redissonClient) {
        this.lockInfo = lockInfo;
        this.redissonClient = redissonClient;
    }

    @Override
    public boolean acquire() {
        try {
            rLock = redissonClient.getLock(lockInfo.getName());
            return rLock.tryLock(lockInfo.getWaitTime(), lockInfo.getLeaseTime(), TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public void release() {

        if (rLock.isHeldByCurrentThread()) {
            rLock.unlinkAsync();
        }
    }
}
