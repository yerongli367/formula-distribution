package com.formula.distribution.lock.entity;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/22
 * @introduce 锁类型
 **/
public interface LockType {

    /**
     * 可重入锁
     */
    String REENTRANT = "Reentrant";
    /**
     * 公平锁
     */
    String FAIR = "Fair";
    /**
     * 读锁
     */
    String READ = "Read";
    /**
     * 写锁
     */
    String WRITE = "Write";


}
