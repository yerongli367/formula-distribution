package com.formula.distribution.lock.lock;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/25
 * @introduce
 **/
public interface LockInterface {

    /**
     * 获取锁
     *
     * @return
     */
    boolean acquire();

    /**
     * 释放锁
     */
    void release();

}
