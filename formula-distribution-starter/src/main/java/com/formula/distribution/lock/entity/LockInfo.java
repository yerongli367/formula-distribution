package com.formula.distribution.lock.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/22
 * @introduce
 **/
@Data
@Builder
@AllArgsConstructor
public class LockInfo {

    /**
     * 锁类型
     */
    private String lockType;

    /**
     * 锁名称
     */
    private String name;

    /**
     * 尝试加锁,最多等待时间
     */
    private long waitTime;

    /**
     * 上锁之后 XXX 秒解锁
     */
    private long leaseTime;

}
