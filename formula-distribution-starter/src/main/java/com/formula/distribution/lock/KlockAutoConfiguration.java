package com.formula.distribution.lock;

import com.formula.distribution.lock.config.LockConfig;
import com.formula.distribution.lock.core.BusinessKeyProvider;
import com.formula.distribution.lock.core.LockAspect;
import com.formula.distribution.lock.core.LockInfoProvider;
import com.formula.distribution.lock.lock.LockFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/26
 * @introduce
 **/
@Configuration
@AutoConfigureAfter(RedisAutoConfiguration.class)
@EnableConfigurationProperties(LockConfig.class)
@Import({LockAspect.class})
public class KlockAutoConfiguration {


    @Bean
    public LockInfoProvider lockInfoProvider() {
        return new LockInfoProvider();
    }


    @Bean
    public BusinessKeyProvider businessKeyProvider() {
        return new BusinessKeyProvider();
    }

    @Bean
    public LockFactory lockFactory() {
        return new LockFactory();
    }
}
