package com.formula.distribution.lock.annotation;

import com.formula.distribution.lock.entity.LockType;

import java.lang.annotation.*;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/22
 * @introduce 锁注解
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Lock {

    /**
     * 锁的名称
     *
     * @return
     */
    String name() default "";


    /**
     * 锁类型
     *
     * @return
     */
    String Lockype() default LockType.REENTRANT;


    /**
     * 尝试加锁,最多等待时间
     *
     * @return
     */
    long waitTime() default Long.MIN_VALUE;


    /**
     * 上锁之后 XXX 秒解锁
     *
     * @return
     */
    long leaseTime() default Long.MIN_VALUE;

    /**
     * 自定义key
     *
     * @return
     */
    String[] keys() default {};

}
