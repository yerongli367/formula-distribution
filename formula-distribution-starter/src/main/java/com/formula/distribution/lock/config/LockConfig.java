package com.formula.distribution.lock.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/22
 * @introduce
 **/
@Data
@ConfigurationProperties(prefix = LockConfig.PREFIX)
public class LockConfig {
    public static final String PREFIX = "spring.formula.lock";

    private long waitTime = 60;
    private long leaseTime = 60;

}
