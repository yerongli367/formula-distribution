package com.formula.distribution.encrypt.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Period;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/14 17:43
 * @introduce
 **/
@ConfigurationProperties(prefix = "spring.formula.encrypt")
public class EncryptProperties {


    /**
     * 加密的key
     */
    private String key;

    private String charset = "UTF-8";

    /**
     * 开启调试模式,调试模式下不进行加密操作
     */
    private boolean debug = false;


    public String getKey() {
        return key.trim();
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }
}
