package com.formula.distribution.encrypt.encrypt;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/14 17:51
 * @introduce
 **/
public abstract class AbstractEncrypt {


    /**
     * 加密
     *
     * @param content
     * @param encryptKey
     * @return
     */
    public abstract String encrypt(String content, String encryptKey) throws Exception;


    /**
     * 解密操作
     *
     * @param encryptStr
     * @param decryptKey
     * @return
     */
    public abstract String decrypt(String encryptStr, String decryptKey) throws Exception;
}
