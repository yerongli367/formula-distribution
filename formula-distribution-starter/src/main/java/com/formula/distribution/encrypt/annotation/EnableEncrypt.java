package com.formula.distribution.encrypt.annotation;

import com.formula.distribution.encrypt.config.EnableEncryptConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/14 14:17
 * @introduce 开启加密
 **/
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(EnableEncryptConfiguration.class)
public @interface EnableEncrypt {
}
