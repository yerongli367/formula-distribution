package com.formula.distribution.encrypt.config;

import com.formula.distribution.encrypt.advice.EncryptRequestBodyAdvice;
import com.formula.distribution.encrypt.advice.EncryptResponseBodyAdvice;
import com.formula.distribution.encrypt.encrypt.AbstractEncrypt;
import com.formula.distribution.encrypt.encrypt.Impl.AESEncryptImpl;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/15 14:40
 * @introduce
 **/
@Component
@EnableAutoConfiguration
@EnableConfigurationProperties(EncryptProperties.class)
@Configuration
@ComponentScan(basePackages = "com.formula.distribution.encrypt")
public class EnableEncryptConfiguration {


    /**
     * 配置请求解密
     *
     * @return
     */
    @Bean
    public EncryptRequestBodyAdvice encryptRequestBodyAdvice() {
        return new EncryptRequestBodyAdvice();
    }


    @Bean
    public EncryptResponseBodyAdvice encryptResponseBodyAdvice() {
        return new EncryptResponseBodyAdvice();
    }


    @Bean("encrypt")
    public AbstractEncrypt abstractEncrypt() {
        return new AESEncryptImpl();
    }

}
