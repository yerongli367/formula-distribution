package com.formula.distribution.encrypt.exception;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/15 15:30
 * @introduce 加解密  自定义异常
 **/
public class EncryptException extends RuntimeException {

    private String message;

    public EncryptException(String message) {
        super(message);
        this.message = message;
    }

    public EncryptException() {
        super();
    }


    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}