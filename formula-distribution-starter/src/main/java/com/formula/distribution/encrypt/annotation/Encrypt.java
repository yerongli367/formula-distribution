package com.formula.distribution.encrypt.annotation;

import java.lang.annotation.*;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/14 14:13
 * @introduce 加密的注解
 **/
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Encrypt {
}
