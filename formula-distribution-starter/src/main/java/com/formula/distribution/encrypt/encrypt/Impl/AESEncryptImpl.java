package com.formula.distribution.encrypt.encrypt.Impl;

import com.formula.distribution.encrypt.encrypt.AbstractEncrypt;
import com.formula.distribution.encrypt.utils.AesEncryptUtils;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/12/14 18:02
 * @introduce 使用 aes 进行加解密
 **/
public class AESEncryptImpl extends AbstractEncrypt {
    @Override
    public String encrypt(String content, String encryptKey) throws Exception {

        return AesEncryptUtils.aesEncrypt(content, encryptKey);
    }

    @Override
    public String decrypt(String encryptStr, String decryptKey) throws Exception {
        return AesEncryptUtils.aesDecrypt(encryptStr, decryptKey);
    }
}
