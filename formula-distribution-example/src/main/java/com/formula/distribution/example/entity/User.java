package com.formula.distribution.example.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/11/29 9:49
 * @introduce
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    private static final long serialVersionUID = -1867225188649663906L;

    private Integer id;

    private String name;

    private Integer age;

    private String address;


}
