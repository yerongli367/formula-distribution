package com.formula.distribution.example.service;

import com.formula.distribution.example.entity.User;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/11/29 9:52
 * @introduce
 **/
public interface IUserService {


    User findById(Integer id);

    User findById2(Integer id);
//
//    User findById3(Integer id);
//
//    User findById4(Integer id);
//
//    User findById5(Integer id);


    String getValue(String params);

    String getValue(Integer id, String username);

    String getValue(User user);
}
