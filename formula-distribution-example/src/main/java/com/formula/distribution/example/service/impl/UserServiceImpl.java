package com.formula.distribution.example.service.impl;

import com.formula.distribution.cache.annotation.CacheExpireTime;
import com.formula.distribution.example.entity.User;
import com.formula.distribution.example.mapper.UserMapper;
import com.formula.distribution.example.service.IUserService;
import com.formula.distribution.lock.annotation.Lock;
import com.formula.distribution.lock.annotation.LockParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/11/29 9:53
 * @introduce
 **/
@Service
@CacheConfig(cacheNames = "user")
@CacheExpireTime(value = 100000L)
@Slf4j
public class UserServiceImpl implements IUserService {


    @Autowired
    UserMapper userMapper;

    @Override
    @Cacheable(value = "findbyid1")
//    @CacheExpireTime(value = 100L)
    public User findById(Integer id) {
        return userMapper.findById(id);
    }

    @Override

    public User findById2(Integer id) {

        log.info("{} 有人进门了", System.currentTimeMillis());
        try {
            Thread.sleep(300L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return userMapper.findById(id);
    }

    @Override
    @Lock(keys = {"#params"}, waitTime = 10, leaseTime = 10)
    public String getValue(String params) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "success";
    }

    @Override
    @Lock(keys = {"#id"})
    public String getValue(Integer id, @LockParam String username) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "success";
    }

    @Override
    @Lock(keys = {"#user.name", "#user.id"})
    public String getValue(User user) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "success";
    }
//
//    @Override
//    @Caching
//    @Cacheable(value = "findbyid3")
//    @CacheExpireTime(value = 4000, min = 111L, max = 100L)
//    public User findById3(Integer id) {
//        return userMapper.findById(id);
//    }
//
//    @Override
//    @Cacheable(value = "findbyid4")
//    @CacheExpireTime(value = 3000, min = 111L, max = 100L)
//    public User findById4(Integer id) {
//        return userMapper.findById(id);
//    }
//
//    @Override
//    @Cacheable(value = "findbyid5")
//    @CacheExpireTime(value = 2000, min = 111L, max = 100L)
//    public User findById5(Integer id) {
//        return userMapper.findById(id);
//    }

}
