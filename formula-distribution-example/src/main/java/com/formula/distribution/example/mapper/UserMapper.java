package com.formula.distribution.example.mapper;

import com.formula.distribution.example.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author:luyanan
 * @email:luyanan0718@163.com
 * @date 2018/11/29 9:48
 * @introduce
 **/
@Mapper
public interface UserMapper {


    User findById(@Param("id") Integer id);
}
