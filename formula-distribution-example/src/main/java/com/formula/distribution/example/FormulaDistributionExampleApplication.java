package com.formula.distribution.example;

import com.formula.distribution.encrypt.annotation.EnableEncrypt;
import com.formula.distribution.limit.annotation.EnableRateLimit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
@EnableRateLimit
@EnableEncrypt
public class FormulaDistributionExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(FormulaDistributionExampleApplication.class, args);
    }
}
