package com.formula.distribution.example;

import com.alibaba.fastjson.JSON;
import com.formula.distribution.encrypt.utils.AesEncryptUtils;
import com.formula.distribution.example.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


public class FormulaDistributionExampleApplicationTests {

    @Test
    public void contextLoads() throws Exception {

        User user = new User();
        user.setAddress("11111");
        user.setAge(12);
        user.setName("222222222");
        String d7b85f6e214abcda = AesEncryptUtils.aesEncrypt(JSON.toJSONString(user), "d7b85f6e214abcda");

        System.out.println(d7b85f6e214abcda);
    }

}
